import base64

import asyncpgsa
from aiohttp import web

# TODO: Раскомментировать, когда установлю пакет
# from cryptography import fernet
# from aiohttp_session import setup, get_session, session_middleware
# from aiohttp_session.cookie_storage import EncryptedCookieStorage

import aiohttp_jinja2, jinja2

from .routes import setup_routes


async def create_app(config: dict):
    app = web.Application()

    # TODO: Раскомментировать, когда установлю пакет
    # fernet_key = fernet.Fernet.generate_key()
    fernet_key = ''
    secret_key = base64.urlsafe_b64decode(fernet_key)
    # setup(app, EncryptedCookieStorage(secret_key))

    app['config'] = config
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.PackageLoader('manager', 'templates'),
    )

    setup_routes(app)
    app.on_startup.append(on_start)
    app.on_cleanup.append(on_shutdown)

    return app


async def on_start(app):
    config = app['config']
    app['db'] = await asyncpgsa.create_pool(dsn=config['database_url'])


async def on_shutdown(app):
    await app['db'].close()
