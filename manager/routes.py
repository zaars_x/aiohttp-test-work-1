from .views import frontend

def setup_routes(app):
    app.router.add_route('GET', '/', frontend.index)
    app.router.add_route('GET', '/register', frontend.Register)
    app.router.add_route('POST', '/register', frontend.Register)
    app.router.add_route('GET', '/login', frontend.Login)
    app.router.add_route('POST', '/login', frontend.Login)

