import hashlib

import bcrypt
from sqlalchemy.sql import select
import aiohttp
from aiohttp import web
from aiohttp_jinja2 import template

from .. import db

@template('index.html')
async def index(request):
    site_name = request.app['config'].get('site_name')
    return {
        'site_name': site_name,
    }



async def get_salted_pwd(pwd, salt):
    return hashlib.md5((pwd + salt).encode('utf-8')).hexdigest()


class Register(web.View):

    @template('register.html')
    async def get(self):
        return {}

    async def post(self):

        data = await self.request.post()

        email = data['email']
        pwd = data['password']
        pwd2 = data['password_2']

        errors = []
        if pwd != pwd2:
            errors.append('Passwords do not match')

        async with self.request.app['db'].acquire() as conn:
            # db.users.select().where(db.users.c.email == email).limit(1)
            # query = select([db.users])
            query = db.users.select().where(db.users.c.email == email).limit(1)
            result = await conn.fetch(query)

        if result:
            errors.append('Cannot use this email')
            return web.json_response(data=dict(errors=errors))


        salt = bcrypt.gensalt().decode('ascii')
        salted_pwd = await get_salted_pwd(pwd, salt)

        async with self.request.app['db'].acquire() as conn:
            query = db.users.insert().values(email=email, password=salted_pwd, salt=salt)
            result = await conn.fetch(query)

        return aiohttp.web.Response(text=str(result))



class Login(web.View):

    @template('login.html')
    async def get(self):
        return {}

    async def post(self):

        data = await self.request.post()

        email = data['email']
        pwd = data['password']

        errors = []

        async with self.request.app['db'].acquire() as conn:
            query = db.users.select().where(db.users.c.email == email).limit(1)
            result = await conn.fetch(query)

        err_str = 'Email or password is wrong or user does not exists'
        if not result:
            errors.append(err_str)
        else:
            user = result[0]
            salted_pwd = await get_salted_pwd(pwd, user['salt'])

            if user['password'] != salted_pwd:
                errors.append(err_str)

        return aiohttp.web.Response(text=str(errors))