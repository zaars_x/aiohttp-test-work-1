CREATE TABLE users (
    id serial primary key,
    email varchar (255) not null,
    password varchar (255) not null,
    salt varchar (255) not null,
);


CREATE TABLE links (
    id serial primary key,
    title varchar (255) not null,
    link varchar (255) not null,
    user_id int not null,
);